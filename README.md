Filesystem Hierarchy Standard (FHS)
==============================


**Binaries**
`/bin` and `/sbin`  are for vital programs for the OS; sbin being for administrators only.

**Data**
`/var` is for living data for programs. It can be cache data, spool data, temporary data (unless it's in `/tmp`, which is wiped at every reboot).

**Programs (standard)**
`/usr/local` is for locally installed programs. Typically, programs that were not packaged for the OS. Programs installed manually by the administrator (using for example ./configure && make && make install) as well as administrator scripts.

**Programs (less standard)**
`/opt` is for programs that are not packaged and don't follow the standards. You'd just put all the libraries there together with the program. It's often a quick & dirty solution, but it can also be used for programs that are made by yourself and for which you wish to have a specific path. You can make your own path (e.g. /opt/yourcompany) within it, and in this case you are encouraged to register it as part of the standard paths.

**Configurations**
`/etc` should not contain programs, but rather configurations.
If your programs are specific to the services provided by the service, /srv can also be a good location for them. For example, I prefer to use /srv/www for websites rather than /var/www to make sure the directory will only contain data I added myself, and nothing that comes from software packages.



Read more: http://www.pathname.com/fhs/